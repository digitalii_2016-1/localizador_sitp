var com = require("serialport");

var serialPort = new com.SerialPort("/dev/ttyUSB0", {                                  

    baudrate: 115200,                                                                 

    dataBits: 8,                                                                      

    parity: 'none',                                                                   

    bufferSize: 256,                                                                  

    flowControl: false,                                                               

    parser: com.parsers.readline("\n")                                                 

  });                                                                                 

serialPort.on('open',function() {                                                     

  console.log('Port open');

  getOK();                                                           

});                          

serialPort.on('data', function(data) {                                                                           

   console.log(data);                                                                                     

            });  

function getOK()

{

  //  console.log("Write");

    serialPort.write("AT+CSQ\r\n");

    setTimeout(getOK, 1000);

}


