`timescale 1ns / 1ps

module GSM(
		output TX_GSM,
		input RX_GSM,
		input clk,
		output reset,
		output TX,
		input RX
    );
assign  TX = RX_GSM;
assign TX_GSM = RX;

//RESET DEL GPS
reg clkb=0;
reg f=0;
reg [26:0] counter10;
always @(posedge clk)
begin	
//Frecuencia de 10Hz para mostrar los segmentos al tiempo	
		if (f==0)
			begin		
			if(counter10==26'd4_999_999)
				begin
				clkb<=~clkb;
				f<=1;
				end
			else 
				begin
				counter10<=counter10+1;
				end
			end
end

assign reset = clkb;

endmodule
