#ifndef SPIKEHW_H
#define SPIKEHW_H

#define PROMSTART 0x00000000
#define RAMSTART  0x00000800
#define RAMSIZE   0x400
#define RAMEND    (RAMSTART + RAMSIZE)

#define RAM_START 0x40000000
#define RAM_SIZE  0x04000000

#define FCPU      100000000

#define UART_RXBUFSIZE 32

/****************************************************************************
 * Types
 */
typedef unsigned int  uint32_t;    // 32 Bit
typedef signed   int   int32_t;    // 32 Bit

typedef unsigned short  uint16_t;    // 16 Bit
typedef signed   short   int16_t;    // 16 Bit

typedef unsigned char  uint8_t;    // 8 Bit
typedef signed   char   int8_t;    // 8 Bit

typedef int bool; //Definition boolean values
#define true 1
#define false 0

#define SIZE_GSM 150
#define TSURL "AT+HTTPPARA=\"URL\",\"api.thingspeak.com/update?key=U8SH4ZCD76CS44W6&field1=___&field2=----&field3=@@@@@@@@&field4=********&field5=####\"\r\n\0"
/****************************************************************************
 * Interrupt handling
 */
typedef void(*isr_ptr_t)(void);

void     irq_enable();
void     irq_disable();
void     irq_set_mask(uint32_t mask);
uint32_t irq_get_mak();

void     isr_init();
void     isr_register(int irq, isr_ptr_t isr);
void     isr_unregister(int irq);

/****************************************************************************
 * General Stuff
 */
void     halt();
void     jump(uint32_t addr);


/****************************************************************************
 * Timer
 */
#define TIMER_EN     0x08    // Enable Timer
#define TIMER_AR     0x04    // Auto-Reload
#define TIMER_IRQEN  0x02    // IRQ Enable
#define TIMER_TRIG   0x01    // Triggered (reset when writing to TCR)

typedef struct {
	volatile uint32_t tcr0;
	volatile uint32_t compare0;
	volatile uint32_t counter0;
	volatile uint32_t tcr1;
	volatile uint32_t compare1;
	volatile uint32_t counter1;
} timer_t;

void nsleep(uint32_t nsec);


void tic_init();


/***************************************************************************
 * GPIO0
 */
typedef struct {
	volatile uint32_t gpio_i;
	volatile uint32_t gpio_o;
	volatile uint32_t gpio_dir;
	
} gpio_t;


char read_data ();
void write_data(char c);
void write_dir(char c);


/***************************************************************************
 * UART0
 */
#define UART_DR   0x01                    // RX Data Ready
#define UART_ERR  0x02                    // RX Error
#define UART_BUSY 0x10                    // TX Busy

typedef struct {
   volatile uint32_t ucr;
   volatile uint32_t rxtx;
} uart_t;

void uart_init();
void uart_putchar(int c);
void uart_putstr(uint32_t *str);
int uart_getchar();

/********************************************
	GPS- Neccesary Functions 
*********************************************/
//Functions GPS UART
void uart_initGPS();
void uart_putcharGPS(int c);
void uart_putstrGPS(int *str);
int uart_getcharGPS();
void KeepInformation(uint32_t *p, uint32_t *latD,uint32_t *latM,uint32_t *lonD,uint32_t *lonM);
void SaveInfo(char *ts,uint32_t *purl, uint32_t *latD,uint32_t *latM,uint32_t *lonD,uint32_t *lonM,uint32_t *peso);
// Funciones Auxiliares
void Fill_String(uint32_t *d, int size);

/********************************************
	GSM SIM800L - Neccesary Functions 
*********************************************/
//Functions GSM UART
void uart_initGSM();
void uart_putcharGSM(int c);
void uart_putstrGSM(uint32_t *str);
int uart_getcharGSM();
int uart_getstrGSM(uint32_t *str, char *pchar,uint8_t line);
void uart_changeComand(char *str, uint32_t *p3);
void ClearComand(uint32_t *pclear);
int wait(uint32_t time);
void sendComand(char *str,uint32_t *p3);
void SendURL(uint32_t *purl,uint32_t *str,char *pchar,uint8_t line);
int uart_compare(uint32_t *p4, uint32_t *p5 );
void shutDownGPRS(uint32_t *p3, uint32_t *p5);
void ClearChar(char *pclear);
bool Confirm(char *psend,uint32_t *pcopy,uint32_t *p5,char *pcheck,uint8_t line);
/***************************************************************************
 * SPI0
 */

typedef struct {
   volatile uint32_t rxtx;
   volatile uint32_t nop1;
   volatile uint32_t cs;
   volatile uint32_t nop2;
   volatile uint32_t divisor;
} spi_t;

void spi_init();
void spi_putchar(char c);
char spi_getchar();



/***************************************************************************
 * Pointer to actual components
 */
extern timer_t  *timer0;
extern uart_t   *uart0;
extern uart_t   *uart1;		//Estructura GPS
extern uart_t   *uart2;		//Estructura GSM  
extern gpio_t   *gpio0;
extern uint32_t *sram0; 

#endif // SPIKEHW_H
