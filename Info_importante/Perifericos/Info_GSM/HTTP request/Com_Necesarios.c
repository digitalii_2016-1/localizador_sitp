AT
AT+CFUN?
AT+CPIN?
AT+CIPSHUT
AT+CGATT=0
AT+CGATT=1
AT+CSTT="internet.comcel.com.co","comcel","comcel"
AT+CSTT="moviletb.net.co","etb","etb"
AT+CSTT="web.colombiamovil.com.co","",""
AT+CIICR
AT+HTTPINIT
AT+HTTPPARA="CID",1
AT+HTTPPARA="URL","api.thingspeak.com/update?key=U8SH4ZCD76CS44W6&field1=3.28"
AT+SAPBR=3,1,"APN","internet.comcel.com.co"
AT+SAPBR=3,1,"USER","comcel"
AT+SAPBR=3,1,"PWD","comcel"

AT+SAPBR=3,1,"APN","moviletb.net.co"
AT+SAPBR=3,1,"USER","etb"
AT+SAPBR=3,1,"PWD","etb"

AT+SAPBR=1,1
AT+HTTPACTION=0

sendComand("AT\r\n",p3,0);	// Atention	
line=2	"OK\r\n"

sendComand("AT+CFUN?\r\n",p3,0); //Verificar funcionamiento 
line=4	"OK\r\n"

sendComand("AT+CPIN?\r\n",p3,0); //Verifica si la SIMCARD necesita PIN
line=4	"OK\r\n"

sendComand("AT+CIPSHUT\r\n",p3,0); //Desactivar el contexto del GPRS
line=2	"SHUT OK\r\n"

sendComand("AT+CGATT=0\r\n",p3,0); //Desconectarse de la red GPRS
line=2	"OK\r\n"

sendComand("AT+CGATT=1\r\n",p3,0); //Vincularse a la red GPRS
line=2	"OK\r\n"

sendComand("AT+CSTT=\"moviletb.net.co\",\"etb\",\"etb\"\r\n",p3,0); //Establecer APN, Usuario y Contraseña de la red
line=2	"OK\r\n"

sendComand("AT+CIICR\r\n",p3,0); //Activar una conexión inalámbrica con GPRS
line=2	"OK\r\n"

sendComand("AT+CIFSR\r\n",p3,0); //Obtener dirección IP local
line=1

sendComand("AT+HTTPINIT\r\n",p3,0); //Inicializar servicio HTTP
line=1

sendComand("AT+HTTPPARA=\"CID\",1\r\n",p3,0); //Configurar parámetros HTTP - Identificador del perfil portador
line=1

sendComand("AT+HTTPPARA=\"URL\",\"api.thingspeak.com/update?key=U8SH4ZCD76CS44W6&latitude=3.28&longitude=-60.25&field1=3.28&field2=-60.25\"\r\n",p3,0); // Uso de ThingSpeak
line=1

sendComand("AT+SAPBR=3,1,\"APN\",\"moviletb.net.co\"\r\n",p3,0); //Ajustes de portador para aplicaciones basadas en IP (Configurar parámetros de portador, portador conectado)
line=1

sendComand("AT+SAPBR=3,1,\"USER\",\"etb\"\r\n",p3,0);
line=1

sendComand("AT+SAPBR=3,1,\"PWD\",\"etb\"\r\n",p3,0);
line=1

sendComand("AT+SAPBR=1,1\r\n",p3,0); //Ajustes de portador para aplicaciones basadas en IP (Portador abierto, Portador conectado)
line=1

sendComand("AT+HTTPACTION=0\r\n",p3,0); //Configurar método de la acción HTTP -(0 - GET - 200 OK)
line=1

