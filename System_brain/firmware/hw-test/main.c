#include "soc-hw.h"

uint32_t GPS[30];
uint32_t Comando[SIZE_GSM];
uint32_t ASCII[SIZE_GSM];   		//Declaración de la cadena
uint32_t ans[SIZE_GSM];
uint32_t x;

//Declaracion de apuntadores
//GPS pointers
uint32_t *p, *p1, *p2;

//Save Location
uint32_t laD[4],laM[9],loD[5],loM[9];
uint32_t *latD,*latM,*lonD,*lonM;

// URL Definition with a pointer URL
uint32_t URL[SIZE_GSM];
uint32_t *purl;

//Cell Information
uint32_t Weight[4]={'4','4','2','1'};
uint32_t *peso;

//GSM Pointer
uint32_t *p3;	//apuntador al comando AT 
uint32_t *p4;	//Apuntador para la respuesta esperada del GSM
uint32_t *p5; // apuntador para  guardar la respuesta del GSM 


uint32_t rx;
uint8_t j;
bool FindPosition=false;
bool FindCharacter=false;
bool y=false;
// Local variables


uint32_t Info_GPS[26];

int main()
{	
	peso=&Weight[0];
	p3=&ASCII[0];
	p4=&Comando[0];	
	p5=&ans[0];
	purl=&URL[0];
	p1=&GPS[0];	
	p2=&GPS[29];
	p=&Info_GPS[0];latD=&laD[0];latM=&laM[0];lonD=&loD[0];lonM=&loM[0]; // GSM pointer to array
	Fill_String(p,8);
	Fill_String(latD,4);Fill_String(latM,9);	// Clean the Latitude array
	Fill_String(lonD,5);Fill_String(lonM,9);	// Clean the Longitude array
	uart_putchar('\n');
	uart_putchar('\r');
	p1=&GPS[0];
	j=0;
	while (FindPosition==false)
	{	
		rx=uart_getcharGPS();
		if(rx=='L'){
			FindCharacter=true;	
		}
		if(FindCharacter)
		{
			
			*p1=rx;
			p1++;
			if(p1==p2)
			{
				p1=&GPS[0];
				if(*(p1+3)!=',')
				{			
					FindPosition=true;
				}
				else
				{
					FindCharacter=false;
				}			
			}
		}
		
	}
	p1=&GPS[0];
	j=0;

	uart_putchar('\n');
	uart_putchar('\r');

	//SAVE INFORMATION FROM GPS IN OTHER ARRAY
	for(j=0; j<26; j++){
		if(j==2 || j==16){
			Info_GPS[j]=32;
		}else if((j<3) || (j==25)){
			Info_GPS[j]=GPS[j+3];
		}else if(j>10){
			Info_GPS[j]=GPS[j+3];
			if (j>16){
			Info_GPS[j]=GPS[j+2];
			}
		} else {
			Info_GPS[j]=GPS[j+2];
		}
	}
	
	KeepInformation(p,latD,latM,lonD,lonM);
	SaveInfo(TSURL,purl,latD,latM,lonD,lonM,peso);
	uart_putstr(purl);
	
	Confirm("AT\r\n",p3,p5,"OK\r\n",2);	
	Confirm("AT+CFUN?\r\n",p3,p5,"OK\r\n",4);
	Confirm("AT+CPIN?\r\n",p3,p5,"OK\r\n",4);		
	shutDownGPRS(p3,p5);
	Confirm("AT+CGATT=1\r\n",p3,p5,"OK\r\n",2);	
	Confirm("AT+CSTT=\"internet.comcel.com.co\",\"comcel\",\"comcel\"\r\n",p3,p5,"OK\r\n",2);
	Confirm("AT+CIICR\r\n",p3,p5,"OK\r\n",2);
	Confirm("AT+HTTPINIT\r\n",p3,p5,"OK\r\n",2);	//Obtener dirección IP local
	Confirm("AT+HTTPPARA=\"CID\",1\r\n",p3,p5,"OK\r\n",2);	//Configurar parámetros HTTP - Identificador del perfil portador
	SendURL(purl,p5,"OK\r\n",2);
	
	//Ajustes de portador para aplicaciones basadas en IP (Configurar parámetros de portador, portador conectado)	
	Confirm("AT+SAPBR=3,1,\"APN\",\"internet.comcel.com.co\"\r\n",p3,p5,"OK\r\n",2); 
	Confirm("AT+SAPBR=3,1,\"USER\",\"comcel\"\r\n",p3,p5,"OK\r\n",2);
	Confirm("AT+SAPBR=3,1,\"PWD\",\"comcel\"\r\n",p3,p5,"OK\r\n",2);
	Confirm("AT+SAPBR=1,1\r\n",p3,p5,"OK\r\n",2);
	Confirm("AT+HTTPACTION=0\r\n",p3,p5,"+HTTPACTION: 0,200,1\r\n",4);//Configurar método de la acción HTTP -(0 - GET - 200 OK)	
	shutDownGPRS(p3,p5);	
	return 0;
}
