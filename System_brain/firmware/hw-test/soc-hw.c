#include "soc-hw.h"

uart_t  *uart0  = (uart_t *)   		0x10000000;
uart_t  *uart1  = (uart_t *)   		0x20000000;
uart_t  *uart2  = (uart_t *)   		0x30000000;
gpio_t  *gpio0  = (gpio_t *)   		0x40000000;
timer_t  *timer0  = (timer_t *) 	0x50000000;

isr_ptr_t isr_table[32];

void tic_isr();
/***************************************************************************
 * IRQ handling
 */
void isr_null()
{
}

void irq_handler(uint32_t pending)
{
	int i;

	for(i=0; i<32; i++) {
		if (pending & 0x01) (*isr_table[i])();
		pending >>= 1;
	}
}

void isr_init()
{
	int i;
	for(i=0; i<32; i++)
		isr_table[i] = &isr_null;
}

void isr_register(int irq, isr_ptr_t isr)
{
	isr_table[irq] = isr;
}

void isr_unregister(int irq)
{
	isr_table[irq] = &isr_null;
}

/***************************************************************************
 * UART Functions
 */

int uart_getchar()
{   
	while (! (uart0->ucr & UART_DR)) ;
	return uart0->rxtx;
}


void uart_putchar(int c)
{
	while (uart0->ucr & UART_BUSY) ;
	uart0->rxtx = c;
}

void uart_putstr(uint32_t *str)
{
	uint32_t *c = str;
	while(*c) {
		uart_putchar(*c);
		c++;
	}
}

/***************************************************************************
 * TIMER0 Functions
 */

uint32_t period0;
uint32_t period0_1;
uint32_t period0_2;
uint32_t frec_aux0;

void period_act0(uint32_t frecuencia, uint8_t *table)
{
	frec_aux0 = frecuencia;
	period0 = (2*FCPU/frecuencia);
	period0_1 = (period0*(*table))/100;
	period0_2 = period0 - period0_1;
}

void enableTimer0()
{
	timer0->compare0 = period0_1;
	timer0->tcr0 = TIMER_AR | TIMER_IRQEN;
	timer0->compare1 = period0_2;
	timer0->tcr1 = TIMER_AR | TIMER_IRQEN;
}

/***************************************************************************
 * UART GPS Functions
 */

int uart_getcharGPS()
{   
	while (! (uart1->ucr & UART_DR)) ;
	return uart1->rxtx;
}

// Funcion Fill of zeros
void Fill_String(uint32_t *d, int size){
	int i;
	for(i=0;i<size;i++){
		*d=0;
		d++;	
	}
}

void KeepInformation(uint32_t *p, uint32_t *latD,uint32_t *latM,uint32_t *lonD,uint32_t *lonM){
	//04 37.43501N,074 04.80651W
	//01234567890123456789012345
	int i=0;	
	if(*(p+11)==78){
		*latD=32;			
	}else{
		*latD=45;
	}
	if(*(p+25)==69){
		*lonD=32;			
	}else{
		*lonD=45;
	}
	*(latD+1)=*p;*(latD+2)=*(p+1); // Latitude Degrees
	*(lonD+1)=*(p+13);*(lonD+2)=*(p+14);*(lonD+3)=*(p+15); //Longitude Degrees
	for(i=0;i<8;i++){
		*(latM+i)=*(p+i+3); 	// Minutes Latitude
		*(lonM+i)=*(p+i+17);	// Minutes Longitude 
	}
}


void SaveInfo(char *ts, uint32_t *purl,uint32_t *latD,uint32_t *latM,uint32_t *lonD,uint32_t *lonM,uint32_t *peso){
	uart_changeComand(ts,purl);	
	uint32_t *laD=latD,*laM=latM,*loD=lonD,*loM=lonM,*pes=peso;
	uint32_t *url = purl;	
	uint32_t i1=0,i2=0,i3=0,i4=0,i5=0;	
	while(*url){
//"AT+HTTPPARA=\"URL\",\"api.thingspeak.com/update?key=U8SH4ZCD76CS44W6&field1=___&field2=----&field3=++++++++&field4=********&field5=####
		if(*url=='_'){
			*url=*(laD+i1);
			i1++;
		}else if(*url=='-'){
			*url=*(loD+i2);
			i2++;
		}else if(*url=='@'){
			*url=*(laM+i3);
			i3++;
		}else if(*url=='*'){
			*url=*(loM+i4);
			i4++;
		}else if(*url=='#'){
			*url=*(pes+i5);
			i5++;
		}
		url++;
	}
}

/*************************************************************************
* UART GSM - Modulo de SIM800L
*/

int uart_getcharGSM()
{   
	while (! (uart2->ucr & UART_DR)) ;
	return uart2->rxtx;
}

void uart_putcharGSM(int c)
{
	while (uart2->ucr & UART_BUSY) ;
	uart2->rxtx = c;
}

void uart_putstrGSM(uint32_t *str)			//Convierte en cadena los caracteres que se quieran enviar
{
	uint32_t *c = str;
	while(*c) {
		uart_putcharGSM(*c);
		c++;
	}
}

int uart_compare(uint32_t *pcom, uint32_t *pGSM )
{   
	int cont=0;
	uint32_t *p4=pcom,*p5=pGSM;
	bool compare=0;
	while (*p4 && cont==0){
		if(*p4==*p5){
			p4++;
			p5++;
			compare=1;
		}else{
			compare=0;
			cont++;
		}
	}
	return compare;
}

void uart_changeComand(char *str, uint32_t *p3)		//Modifica la cadena del comando que se quiere enviar con apuntadores
{
	ClearComand(p3);	
	char *c = str;
	while(*c) {		
			*p3= *c;			
			c++;
			p3++;		
	}
}

void sendComand(char *str,uint32_t *p3)	
{
	char *c = str;
	uart_changeComand(c,p3);	//Escribe en el arreglo
	uart_putstr(p3);	
	uart_putstrGSM(p3);		//Manda al GSM el comando
}

void SendURL(uint32_t *purl,uint32_t *str,char *pchar,uint8_t line){
	uart_putstrGSM(purl);
	uart_getstrGSM(str,pchar,line);
}

void ClearComand(uint32_t *pclear)	//Limpia la cadena
{
	int i=0;
	uint32_t *pc=pclear;
	while(i<SIZE_GSM)
	{
		*pc = 0;
		pc++;
		i++;
	}
}

int uart_getstrGSM(uint32_t *str,char *pchar,uint8_t line)	//Convierte en cadena los caracteres que se quieran enviar
{
	ClearComand(str);	
	uint32_t Compare[SIZE_GSM];	
	uint32_t *pcompare,*c=str;
	bool found=false;
	uint32_t x=0,cont=0,FindString=0;
	pcompare=&Compare[0];
	uart_changeComand(pchar,pcompare); // Pasar el char de entrada a 32bits
	while(found==false) {
		x=uart_getcharGSM();
		*c=x;
		c++;
		if(x=='\n'){
			if(uart_compare(pcompare, str)==1){ //Compara el string que ponemos con lo que le entra del GSM
				found=true;
				uart_putstr(str);
				uart_putchar('\n');				
				FindString=1;
			}
			else{
				cont++;
				c=str;
				ClearComand(c); 
				if(cont==line){
					found=true;
					uart_putchar('\n');
					FindString=0;				
				}
			}
		}
	}
	return FindString;
}

bool Confirm(char *psend,uint32_t *pcopy,uint32_t *p5,char *pcheck,uint8_t line){
	bool confirmation=false;
	int count=0;
	while(confirmation==false && count<5){
		sendComand(psend,pcopy);
		if(uart_getstrGSM(p5,pcheck,line)==1){
			confirmation=true;
			return true;	
		}
		else{
			count++;
		}
	}
	if(count==5){
		return false;	
	}
}

void shutDownGPRS(uint32_t *p3, uint32_t *p5){
	Confirm("AT+CIPSHUT\r\n",p3,p5,"SHUT OK\r\n",2);
	Confirm("AT+CGATT=0\r\n",p3,p5,"OK\r\n",2);
}

int wait(uint32_t time)
{
	uint32_t i=0;
	while(i<time){
		i++; 
	}
	return 0;

}

/***************************************************************************
 * GPIO Functions
 */

char read_data ()
{	
	return gpio0->gpio_i;
	
}

void write_data(char c)
{
	gpio0->gpio_o = c;
}

void write_dir(char c)
{
	gpio0->gpio_dir = c;
}
