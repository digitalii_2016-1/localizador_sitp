5.2.1
Antenna Interface
The basis principle is that, the length of trace between pin output and connector should be as short as possible, in
order to avoid coupling issue. Do not trace RF signal over across the board. Even the RF cable must be put over
the board, it should be far away from SIM card, power ICs.
There are FM antenna interface and GSM antenna interface. If product equips these antennas as well, do pay
attention to the distance between each single antenna.


